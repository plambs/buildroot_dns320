# D-Link DNS320
This repository explain how to install uboot and Linux 5.14 on a D-Link DNS320 using buildroot.

## Thanks
Thanks to Jamie Lentin for the excellent [blog post](https://jamie.lentin.co.uk/devices/dlink-dns325/replacing-firmware/) from where the u-boot and flashing informations came.

## Build
```
git clone git@gitlab.com:plambs/buildroot_dns320.git
cd buildroot_dns320
make dns320_defconfig
make
```
The bootloader, kernel and rootfs can be found in output/images/

## Try U-Boot before flashing it or unbrick the device
You can run u-boot directly from the UART using one of the two following method. \
First close all terminal that open the uart serial port and shutdown the device. \
After that run one of the two command then power the device, the loading should start and end up with a running uboot console.
```
output/build/uboot-master-dns320/tools/kwboot -p -b u-boot.kwb -B115200 -t /dev/ttyUSBx
```
or use [kwuartboot](https://github.com/lentinj/kwuartboot) if it doesn't work with kwboot. \
To do so you only need to change the line "BOOT_FROM nand" to "BOOT_FROM uart" in the file d-link/dns320/kwbimage.cfg. \
Then rebuild it and do:
```
git clone git@gitlab.com:plambs/u-boot_dns320.git
cd u-boot_dns320
make dns320_defconfig
**edit d-link/dns320/kwbimage.cfg**
make
./kwuartboot /dev/ttyUSBx uboot.kwb
```

## Flash U-Boot
Copy u-boot.kwd on a usb key formatted in ext2.

Interrupt uboot with **space** + **1** sequence
```
usb reset ; ext2load usb 0:1 0x1000000 /u-boot.kwb
nand erase 0x000000 0xe0000
nand write 0x1000000 0x000000 0xe0000
reset
```

After reset interrupt the boot and set the minimal env:
```
setenv ethaddr 00:50:xx:xx:xx:xx
setenv mtdparts 'mtdparts=orion_nand:896k(u-boot),128k(u-boot-env),-(root)'
saveenv
```

If you want to use tftp boot set the following env:
```
setenv ipaddr [Our IP address, e.g. 10.150.1.3]
setenv serverip [Where TFTP server is, e.g. 10.150.1.10]
```

## Install rootfs and setup boot env

Copy the content of output/images on a usb key

Boot the device and interrupt uboot, then install the ubifs image:
```
nand erase.part root
ubi part root
ubi create rootfs
usb reset ; ext2load usb 0:1 0x1000000 /rootfs.ubifs
ubi write 0x1000000 rootfs [size read just before in hex]
ubifsmount ubi:rootfs
```

Setup boot env:
```
setenv root_flash 'ubi.mtd=root root=ubi0:rootfs rootfstype=ubifs rw'
setenv load_ubifs 'ubi part root ; ubifsmount ubi:rootfs ; ubifsload 0x1000000 /boot/zImage.kirkwood-dns320'
setenv console 'ttyS0,115200'
setenv optargs ''
setenv bootcmd 'setenv bootargs console=${console} ${optargs} ${mtdparts} ${root_flash} ; run load_ubifs ; bootz 0x1000000'
saveenv
```

## Links
[https://jamie.lentin.co.uk/devices/dlink-dns325/](https://jamie.lentin.co.uk/devices/dlink-dns325/)\
[https://dns323.kood.org/dns-320](https://dns323.kood.org/dns-320)\
[https://github.com/lentinj/kwuartboot](https://github.com/lentinj/kwuartboot)\
[https://github.com/martignlo/DNS-320L](https://github.com/martignlo/DNS-320L)\
[https://www.aboehler.at/doku/doku.php/projects:dns320l](https://www.aboehler.at/doku/doku.php/projects:dns320l)\
[https://wikidevi.wi-cat.ru/D-Link_DNS-320L_rev_A1](https://wikidevi.wi-cat.ru/D-Link_DNS-320L_rev_A1)\
[https://deviwiki.com/wiki/D-Link_DNS-320L_rev_A1](https://deviwiki.com/wiki/D-Link_DNS-320L_rev_A1)\
[https://github.com/scus1/dns320l](https://github.com/scus1/dns320l)\
[https://hackaday.io/project/179084-rebuilding-the-linux-for-dns-320l-nas/details](https://hackaday.io/project/179084-rebuilding-the-linux-for-dns-320l-nas/details)
